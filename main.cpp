// std
#include <iostream>
#include <string>
#include <string_view>
#include <map>
#include <vector>

using namespace std;

// opencv
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;

typedef vector<string_view> stringViewList;
inline stringViewList splitString( std::string_view input_string, const char separator ) 
{
	stringViewList parts;
	size_t part_length = 0;
	while( ( part_length = input_string.find( separator ) ) != input_string.npos ) {
		parts.emplace_back( input_string.data(), part_length );
		input_string.remove_prefix( part_length + 1 );
	}

	if( !input_string.empty() ) {
		parts.push_back( std::move( input_string ) );
	}

	return parts;
}

int main()
{
	string input_line;
	map <string, Mat> images;
	map <string, string> commands_help = {
		{ "load",   "Usage: load <name> <filename>" },
		{ "store",  "Usage: store <name> <filename>" },
		{ "resize", "Usage: resize <from_name> <to_name> <new_width> <new_height>" },
		{ "blur",   "Usage: blur <from_name> <to_name> <size>" }
	};
	while(cin) {
		getline(cin, input_line);
		stringViewList list = splitString(input_line, ' ');

		if (list.size() < 1) {
			continue;
		}

		if (list[0] == "load" || list[0] == "ld") {
			if (list.size() < 3) {
				cout << commands_help["load"] << endl;
				continue;
			}
			string name = string(list[1]);
			string filename = string(list[2]);
			Mat image = imread(filename);

			if(image.empty())
			{
				cout << "Could not read the image: " << filename << endl;
				continue;
			}

			images[name] = image;
			cout << "Success!" << endl;
		} else if (list[0] == "store" || list[0] == "s") {
			if (list.size() < 3) {
				cout << commands_help["store"] << endl;
				continue;
			}
			
			string name = string(list[1]);

			if (!images.count(name)) {
				cout << "Image not found." << endl;
				continue;
			}

			string filename = string(list[2]);
			const Mat &image = images[name];
			bool success = imwrite(filename, image);
			cout << (success ? "File saved." : "Failed!") << endl;
		} else if (list[0] == "resize") {
			if (list.size() < 5) {
				cout << commands_help["resize"] << endl;
				continue;
			}

			string from_name = string(list[1]);
			if (!images.count(from_name)) {
				cout << "Image not found." << endl;
				continue;
			}

			string to_name = string(list[2]);

			int new_width, new_height;
			try {
				new_width = std::stoi(string(list[3]));
				new_height = std::stoi(string(list[4]));
			}
			catch (const invalid_argument& e) {
				cout << "Wrong image size." << endl;
				continue;
			}

			const Mat &image = images[from_name];
			Mat new_image;
			resize(image, new_image, Size(new_width, new_height), 0, 0, CV_INTER_CUBIC);
			images[to_name] = new_image;
			cout << "Success!" << endl;
		} else if (list[0] == "blur") {
			if (list.size() < 4) {
				cout << commands_help["blur"] << endl;
				continue;
			}

			string from_name = string(list[1]);
			if (!images.count(from_name)) {
				cout << "Image not found." << endl;
				continue;
			}

			string to_name = string(list[2]);

			int size;
			try {
				size = std::stoi(string(list[3]));
			}
			catch (const invalid_argument& e) {
				cout << "Wrong size." << endl;
				continue;
			}

			const Mat &image = images[from_name];
			Mat new_image;;
			blur(image, new_image, Size(size, size));
			images[to_name] = new_image;
			cout << "Success!" << endl;
		} else if (list[0] == "help" || list[0] == "h") {
			cout << "Available commands: blur, load, resize, store, help, quit" << endl;
			
			for (auto &pair : commands_help) {
				cout << pair.second << endl;
			}
		} else if (list[0] == "quit" || list[0] == "exit" || list[0] == "q") {
			return 0;
		} else {
			cout << "Unknown command." << endl;
		}
	};
	return 0;
}
