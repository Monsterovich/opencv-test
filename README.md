```
./opencv-conv 
help
Available commands: blur, load, resize, store, help, quit
Usage: blur <from_name> <to_name> <width> <height>
Usage: load <name> <filename>
Usage: resize <from_name> <to_name> <new_width> <new_height>
Usage: store <name> <filename>
load test input.jpg
Success!
resize test test2 300 300
Success!
store test2 output1.jpg
File saved.
blur test test3 5
Success!
store test3 output2.jpg
File saved.
q
```
